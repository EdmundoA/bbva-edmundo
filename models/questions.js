var mongoose = require('mongoose');
var constants = require('../lib/constants');
var Schema = mongoose.Schema;

var question_schema = new Schema({
 question: { type: String, required: true },
 answers: [
  {
   answer: { type: String },
   is_correct: { type: Boolean, default: false },
  },
 ],
 comment: String,
 creator: { type: Schema.Types.ObjectId, ref: 'User' },
 date: { type: Date, default: Date.now },
});

var Question = mongoose.model('Question', question_schema);

module.exports = Question;
