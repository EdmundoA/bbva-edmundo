var mongoose = require('mongoose');
var constants = require('../lib/constants');
var Schema = mongoose.Schema;

var session_schema = new Schema({
 user: { type: Schema.Types.ObjectId, ref: 'User' },
 status: { type: Boolean, required: true },
 date: { type: Date, default: Date.now }
});

var Session = mongoose.model('Session', session_schema);

module.exports = Session;
