var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var team_schema = new Schema({
 team: { type: String, required: true, unique: true },
 quantity: { type: Number, required: true },
 creator: { type: Schema.Types.ObjectId, ref: 'User' },
 date: { type: Date, default: Date.now }
});

var Team = mongoose.model('Team', team_schema);

module.exports = Team;
