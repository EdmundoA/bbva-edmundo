var mongoose = require('mongoose');
var constants = require('../lib/constants');
var Schema = mongoose.Schema;

var user_schema = new Schema({
 username: { type: String, required: true, unique: true },
 name: { type: String, required: true },
 password: { type: String, required: true },
 code: { type: Number, required: true},
 company: { type: String, required: true },
 mail: { type: String, required: true },
 position: { type: String, required: true },
 role: { type: String, enum: constants.ROLES , default: constants.ROLES[2]},
 creator: { type: Schema.Types.ObjectId, ref: 'User' },
 job: { type: String, required: false },
 location: { type: String, required: false },
 gerency: { type: String, required: false },
 leader: { type: Schema.Types.ObjectId, ref: 'User' },
 area: { type: String, required: false },
 dateCreated: { type: Date, default: Date.now },
 dateModify: { type: Date, default: Date.now}
});

var User = mongoose.model('User', user_schema);

module.exports = User;
