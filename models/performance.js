var mongoose = require('mongoose');
var constants = require('../lib/constants');
var Schema = mongoose.Schema;

var performance_schema = new Schema({
 user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
 year: { type: Number, required: true },
 calification: { type: String },
 calibration: { type: String },
 challenges: {
 	challenges_detail: [
	  	{
	  		name: { type: String },
	  		qualify: { type: Number},
	  		score: { type: String },
	  		weight: { type: String },
	  		qualify_calibration: { type: Number},
	  		score_calibration: { type: String },
	  		weight_calibration: { type: String }
	  	}
  	],
  	comments: { type: String },
  	percent: { type: Number },
  	total_qualification: { type: Number },
  	comments_calibration: { type: String },
  	percent_calibration: { type: Number },
  	total_qualification_calibration: { type: Number }
 }, 
 competitions: {
 	competitions_list: [
 		{
 			name: { type: String },
 			description: { type: String },
 			qualify: { type: Number },
 			qualify_calibration: { type: Number },
 			competitions_associated: [
 				{
 					id: { type: Number },
 					name: { type: String },
 					score: { type: String },
 					score_calibration: { type: String }
 				}
 			]
 		}
 	],
 	total_qualification: { type: Number },
 	total_qualification_calibration: { type: Number }
 },
 delivered: { type: Boolean },
 status: { type: Number, required: true },
 qualify_end: { type: Number },
 qualify_end_calibration: { type: Number },
 creator: { type: Schema.Types.ObjectId, ref: 'User' },
 comments_first: { type: String },
 comments_finished: { type: String },
 dateCreated: { type: Date, default: Date.now },
 dateModify: { type: Date, default: Date.now}
});

var Performance = mongoose.model('Performance', performance_schema);

module.exports = Performance;