var express = require('express');
var router = express.Router();
var constants = require('../lib/constants');
var User = require('../models/users');
var Team = require('../models/teams');
var bad_connection = require('../middlewares/bad_connection');
var success_connection = require('../middlewares/success_connection');

router.route('/')
.get(function(req, res){
 User.find({role:constants.ROLES[2]}, function(err, users){
  return success_connection(req, res, 200, users);
 });
})
.post(async function(req, res){
 var queryFindUsers = { $or: [] };
 var users = req.body.users;
 for(var i = 0; i < users.length; i++){
  users[i].role = constants.ROLES[users[i].role];
  queryFindUsers['$or'].push({username:users[i].username});
 }
 var findUsers = await User.find(queryFindUsers);
 if(findUsers.length != 0){
  for(var j = 0; j < findUsers.length; j++){
   var indexDelete = users.findIndex(u => u.username == findUsers[j].username);
   users.splice(indexDelete, 1);
  }
 }
 var resultUsers = await User.insertMany(users);
 return success_connection(req, res, 200, resultUsers);
});

router.post('/login', function(req, res){

	User.findOne({password:req.body.password, username: {$regex: new RegExp('^' + req.body.username.toLowerCase(), 'i')}}, function(err, user){
		return success_connection(req, res, 200, user);
	});
});

module.exports = router;
