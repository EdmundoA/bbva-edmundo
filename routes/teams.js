var express = require('express');
var router = express.Router();
var constants = require('../lib/constants');
var Team = require('../models/teams');
var bad_connection = require('../middlewares/bad_connection');
var success_connection = require('../middlewares/success_connection');

router.route('/')
.get(function(req, res){
 Team.find({}, function(err, teams){
  return success_connection(req, res, 200, teams);
 });
})
.post(async function(req, res){
 var queryFindTeams = { $or : [] };
 var teams = req.body.teams;
 for (var i = 0; i < teams.length; i++) {
  queryFindTeams['$or'].push({team:teams[i].team});
 }
 var findTeams = await Team.find(queryFindTeams);
 if(findTeams.length != 0){
  for(var j = 0; j < findTeams.length; j++){
   var indexDelete = teams.findIndex(t => t.team == findTeams[j].team);
   teams.splice(indexDelete, 1);
  }
 }
 var resultTeams = await Team.insertMany(teams);
 return success_connection(req, res, 200, resultTeams);
})
.put(async function(req, res){
 var dataTeams = req.body.teams;
 var teams = await Team.find({});
 for(var i = 0; i < teams.length; i++){
  var index = updateTeamIndex(dataTeams, teams[i].team);
  if(index != -1){
   teams[i].quantity = dataTeams[index].quantity;
   await teams[i].save();
  }
 }
 teams = await Team.find({});
 return success_connection(req, res, 200, teams);
});

function updateTeamIndex(nteams, team){
 for(var i = 0; i < nteams.length; i++){
  if(nteams[i].team == team){
   return i;
  }
 }
 return -1;
}

module.exports = router;
