var express = require('express');
var router = express.Router();
var constants = require('../lib/constants');
var User = require('../models/users');
var Question = require('../models/questions');
var bad_connection = require('../middlewares/bad_connection');
var success_connection = require('../middlewares/success_connection');

router.route('/')
.get(function(req, res){
 Question.find({}, function(err, questions){
  return success_connection(req, res, 200, questions);
 });
})
.post(async function(req, res){
 var queryFindQuestions = { $or: [] };
 var questions = req.body.questions;
 for(var i = 0; i < questions.length; i++){
  queryFindQuestions['$or'].push({question:questions[i].question});
 }
 var findQuestions = await Question.find(queryFindQuestions);
 if(findQuestions.length != 0){
  for(var j = 0; j < findQuestions.length; j++){
   var indexDelete = questions.findIndex(q => q.question == findQuestions[j].question);
   questions.splice(indexDelete, 1);
  }
 }
 var resultQuestions = await Question.insertMany(questions);
 return success_connection(req, res, 200, resultQuestions);
});

module.exports = router;
