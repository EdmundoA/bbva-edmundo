class Player{
 constructor(name, id, idSocket, room, team){
  this.name = name;
  this.team = team;
  this.idSocket = idSocket;
  this.room = room;
  this.id = id;
  this.correctAnswers = 0;
  this.esteemedPlayers = 0;
 }
}

module.exports = Player;
