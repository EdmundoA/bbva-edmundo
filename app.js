var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var constants = require('./lib/constants');
var mongoose = require('mongoose');
var formidable = require('express-formidable');
var methodOverride = require('method-override');
var http = require('http');
var realtime = require('./lib/realtime');

var users = require('./routes/users');
var teams = require('./routes/teams');
var questions = require('./routes/questions');

var app = express();
var server = http.Server(app);
var port = constants.PORT;

mongoose.connect(constants.MONGO_PATH);
app.use(cors());
app.use(methodOverride('_method'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(formidable.parse({keepExtensions:true}));

var io = realtime(server);

app.get('/', function(req, res){
	console.log('init server');
	res.status(200).json({msg:'Bienvenido a las previas de BBVA'});
});

app.use('/users', users);
app.use('/teams', teams);
app.use('/questions', questions);

server.listen(port, function(){
	console.log('Ready on port %d', server.address().port);
});

module.exports = server;
