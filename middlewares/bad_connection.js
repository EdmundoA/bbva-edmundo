var constants = require('../lib/constants');

module.exports = function(req, res, status, message){
 return res.send({
  status: status, message: constants.ERROR[status], result: message,
 });
}
