var listRooms = [];
var listPlayers = [];
var listTeams = {};
var num = 1;
var max_questions = 5;
var archive_config = 'public/data_Bbva.json';

module.exports = async function(server){
 var io = require('socket.io')(server);
 var Player = require('../classes/player');
 var Team = require('../models/teams');
 var fs = require('fs');

 var dataTeams = await Team.find({});
 var listTeamsIn = [];
 var listTeamsOut = [];

 io.sockets.on("connection", function(socket){
  console.log('se conecto');

  socket.on("onconnect", function(message){
   console.log('se conecto');
   console.log(socket);
  });

  socket.on('updateTeams', function(message){
   Team.find({}, function(err, teams){
    dataTeams = teams;
    console.log('se actualizao la datateams');
    console.log(dataTeams);
   });
  });

  socket.on('createRoom', function(message){
   socket.join('room' + 1, () => {
    listRooms.push({name:'room' + 1, creator:socket.id, throwedQuestions: 0});
    console.log('se creo el cuarto');
    let rooms = Object.keys(socket.rooms);
    console.log('cuartos del usuario');
    console.log(rooms);
   });
  });

  socket.on('joinRoom', function(message){
   console.log(message);
   var existRoom = (listRooms.filter(room => room.name == message.room).length != 0);
   if(existRoom && !existPlayer(message.id)){
    socket.join(message.room, () => {
     console.log('se unio al cuarto');
     var tUser = new Player(message.name, message.id, socket.id, message.room, message.team);
     if(!listTeams[message.team]){
      listTeams[message.team] = {name: message.team, corrects: 0, responses: 0, allResponses: 0, esteemedResponses: 0, players: 0, esteemedPlayers: 0, participation: 0, efficiency: 0, points: 0, secondsUse: 0, promSeconds: 0};
      listTeams[message.team].esteemedPlayers = findPlayersDataTeams(message.team);
     }
     if(listTeams[message.team]){
      listTeams[message.team].players++;
      tUser.esteemedPlayers = listTeams[message.team].esteemedPlayers;
     }
     listPlayers.push(tUser);
     if(listRooms[0]){
      io.sockets.connected[listRooms[0].creator].emit('joiningRoom', tUser);
     }
    });
   }
   else{
    io.sockets.connected[socket.id].emit('dontjoin', 'no existe este cuarto o ya esta registrado');
   }
  });

  socket.on('waitQuestion', function(message){
   socket.broadcast.emit('waitingQuestion', 'esperando');
  });

  socket.on('initializeGame', function(message){
   socket.broadcast.emit('initGame', 'pantalla de inicio');
  });

  socket.on('showAnswers', function(message){
   socket.broadcast.emit('showingAnswers', message);
  });

  socket.on('responseQuestion', function(message){
   if(message.response && existPlayer(message.id)){
    listTeams[message.team].corrects++;
    correctAnswerPlayer(message.id);
   }
   listTeams[message.team].responses++;
   var allResponses = 0;
   for(var team in listTeams){
    allResponses += listTeams[team].responses;
    listTeams[team].secondsUse += message.seconds;
   }
   if(listPlayers.length == allResponses){
    allResponses = 0;
    //endQuestion();
   }
  });

  socket.on('preQuestion', function(message){
   if(message.seconds > 0){
    message.dataTeams = dataTeams;
    message.listTeams = listPlayers;
    socket.broadcast.emit('preQuestion', message);
   }
   else{
    if(listRooms[0]){
     listRooms[0].throwedQuestions++;
    }
    socket.broadcast.emit('showingAnswers', message);
   }
  });

  socket.on('inQuestion', function(message){
   if(message.seconds > 0){
    socket.broadcast.emit('inQuestion', message);
   }
   else{
    endQuestion();
    //socket.broadcast.emit('timeExpired', message);
   }
  });

  socket.on('postQuestion', function(message){
   socket.broadcast.emit('postQuestion', message);
  });

  socket.on('timeExpired', function(message){
   endQuestion();
  });

  socket.on('tableGame', function(message){
   if(listRooms[0]){
    io.sockets.connected[listRooms[0].creator].emit('tableQuestion', {listTeams: listTeams, listTeamsIn: listTeamsIn, listTeamsOut: listTeamsOut});
   }
  });

  socket.on('islogged', function(message){
    if(!existPlayer(message.id)){
      io.sockets.connected[socket.id].emit('nologged', {});
    }
  })

  socket.on("disconnect", function(){
   console.log('se desconecto');
   var exist = isManager(socket.id);
   if(exist){
    clearLists();
   }
   else{
    //deletePlayer(socket.id);
   }
  });
 });

 function endQuestion(){
  for(var team in listTeams){
   listTeams[team].esteemedResponses += listTeams[team].players;
   listTeams[team].allResponses += listTeams[team].responses;
   listTeams[team].secondsUse = ((listTeams[team].players - listTeams[team].responses)*20) + listTeams[team].secondsUse;
   listTeams[team].responses = 0;
  }
  if(listRooms[0] && listRooms[0].throwedQuestions >= max_questions){
   for(var team in listTeams){
    listTeams[team].participation = Math.round(listTeams[team].players/listTeams[team].esteemedPlayers*100*1000)/1000;
    listTeams[team].efficiency = Math.round(listTeams[team].corrects/listTeams[team].esteemedResponses*100*1000)/1000;
    listTeams[team].promSeconds = Math.round(listTeams[team].secondsUse/listTeams[team].esteemedResponses*1000)/1000;
    if(listTeams[team].name == 'SIN EQUIPO' || listTeams[team].name == '-' || listTeams[team].name == ''){}
    else if(listTeams[team].players >= Math.floor(listTeams[team].esteemedPlayers/2) + 1){
     listTeamsIn.push(listTeams[team]);
    }
    else{
     listTeamsOut.push(listTeams[team]);
    }
   }

   listTeamsIn.sort(function(a,b){
    if(a.efficiency == b.efficiency){
     if(a.participation == b.participation){
      return a.promSeconds > b.promSeconds ? -1 : 1;
     }
     return a.participation < b.participation ? -1 : 1;
    }
    return a.efficiency < b.efficiency ? -1 : 1;
   });
   listTeamsIn.reverse();
   for (var i = 0; i < listTeamsIn.length; i++) {
      if(i == 0){
       listTeamsIn[i].points = 2;
      }
      if(i == 1){
       listTeamsIn[i].points = 1;
      }
      if(i == 2){
       listTeamsIn[i].points = .5;
      }
   }
   var rafflePlayers = [];
   for (var ki = listPlayers.length - 1; ki >= 0; ki--) {
     if(listPlayers[ki].team != 'SIN EQUIPO' && listPlayers[ki].team != '-' && listPlayers[ki].team != '' && listPlayers[ki].correctAnswers > 0){
        rafflePlayers.push(listPlayers[ki]);
     }
   }
   io.sockets.connected[listRooms[0].creator].emit('raffle', {listPlayers: rafflePlayers, listTeams: listTeams, listTeamsIn: listTeamsIn, listTeamsOut: listTeamsOut});
   fs.writeFile(archive_config, JSON.stringify(listTeamsIn), 'utf8', function(err){
    if(err){ throw err; return; }
    
   });
  }
  else{
   io.sockets.connected[listRooms[0].creator].emit('waitingNextQuestion', {numQuestion: listRooms[0].throwedQuestions});
  }
 }

 function findPlayersDataTeams(team){
  for(var i = dataTeams.length - 1; i >= 0; i--){
   if(dataTeams[i].team == team){
    return dataTeams[i].quantity;
   }
  }
  return 0;
 }

 function correctAnswerPlayer(id){
  for(var i = listPlayers.length - 1; i >= 0; i--){
   if(listPlayers[i].id == id){
    listPlayers[i].correctAnswers ++;
    return;
   }
  }
 }

 function deletePlayer(socketid){
  for(var i = listPlayers.length - 1; i >= 0; i--){
   if(listPlayers[i].idSocket == socketid){
    io.sockets.connected[listRooms[0].creator].emit('disconnectPlayer', listPlayers[i]);
    deletePlayerInTeams(listPlayers[i].team);
    listPlayers.splice(i, 1);
    return;
   }
  }
 }

 function deletePlayerInTeams(team){
  if(listTeams[team]){
   listTeams[team].players--;
   if(listTeams[team].players <= 0){
    delete listTeams[team];
   }
  }
 }

 function clearLists(){
  for(var l = listRooms.length - 1; l >= 0; l--){
   listRooms.pop();
  }
  for(var i = listPlayers.length - 1; i >= 0; i--){
   listPlayers.pop();
  }
  for(var k = listTeamsIn.length - 1; k >= 0; k--){
   listTeamsIn.pop();
  }
  for(var t = listTeamsOut.length - 1; t >= 0; t--){
   listTeamsOut.pop();
  }
  listTeams = {};
 }

 function existPlayer(id){
  for (var i = listPlayers.length - 1; i >= 0; i--) {
    if(listPlayers[i].id == id){
      return true;
    }
  }
  return false;
 }

 function isManager(socketid){
  for(var exi = listRooms.length - 1; exi >= 0; exi--){
   if(listRooms[exi].creator == socketid){
    return true;
   }
  }
  return false;
 }

 return io;
}
