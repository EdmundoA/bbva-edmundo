function define(name, value){
        Object.defineProperty(exports, name, {
                value: value,
                enumerable: true,
        });
}

define('PORT', 4056);
define('MONGO_PATH', "mongodb://localhost/previas_bbva");
define('ROLES', [
        'Admin',
        'Manager',
        'Player',
]);

define('CONFIRM', {
        200: 'OK',
        201: 'Created',
        209: 'Updated',
        210: 'Deleted',
        211: 'Uploaded',
});

define('ERROR', {
        403: 'Forbidden',
        404: 'Restricted',
        454: 'Duplicated connection',
        470: 'BD Error',
        480: 'Missing parameters',
        491: 'Empty response',
        494: "User doesn't exist",
        495: 'User already exist',
        499: 'Custom error',
});
